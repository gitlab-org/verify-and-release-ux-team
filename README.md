Hey There!

This is the project for the CI/CD UX team.

This project is designed to hold team process discussions, efforts towards team continuity and productivity. Generally a place to have more structured discussions around things we want to do.

Visit [our handbook page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/) to learn who we are and how we work.