## Intro

Now that you completed your Individual Growth Plan (IGP), it's time to plan action items and track progress as a result of the professional development opportunities they're seeking while on GitLab. Use this issue to guide discussions with your manager about your IGP plan, accomplishments, and iterations of your goals.

### How this issue works

It's simple! We'll use this issue to reference, reflect, and iterate on your goals. 

- Look at the Section 3 in your IGP issue and, individually, set 1-2 SMART goals for your career development this year. You can use this [FigJam template](https://www.figma.com/community/file/1104227437175207230).
- Use this issue discuss them with your manager and to understand what your organization’s goals are, and how you can contribute to them through your goals.
- Consider linking issues/MRs to this issue as a way to track actions taken, lessons and skills learned.
    - Set calendar reminders where appropriate so you don’t forget to revisit your plan.
- Synchronous check-ins: Schedule monthly check-in calls (30/45 min duration) with your manager where you can review your progress, discuss challenges, and make adjustments to your plan.
- Asynchronous check-ins: Consider opening a monthly thread in this issue to discuss and record a summary of the progress made for the period, and work with your manager to review your progress, discuss challenges, and make adjustments to your plan.

## Setting SMART goals

[SMART goals](https://www.betterup.com/blog/smart-goals-examples) follow a specific framework to achieve goals: they should be **S**pecific, **M**easurable, **A**chievable, **R**elevant, and **T**ime-bound. You can use the SMART goal framework for short-term and long-term career goals.

<details><summary>Anatomy of SMART goals</summary>

1. **Specific:** Your goals should be limited to a single area. Instead of trying to “be a better person,” try focusing on your listening skills or positive self-talk.
1. **Measurable:** The goal must align with explicit metrics, such as plan your workload every week on Monday or qualifying for a particular certification. This indicates when you’ve accomplished your mission.
1. **Attainable:** The goal needs to be realistic. Maybe you won’t qualify for the Olympic basketball team, but you could improve your free throw percentage.
1. **Relevant:** Goals should move you in the direction of your values, dreams, and ambitions.
1. **Time-bound:** Time-based goals create urgency and encourage efficient time management. There must be a due date for completion so you can pace your efforts.

</details>

Once you’ve checked all these boxes, you’re left with a detailed goal-setting plan that keeps you focused and headed in the right direction. 

## My SMART goals

- Keep the number of goals low and identify opportunities that will help achieve the career development goals you set.
- Connect each SMART goal to a broader career goal. For example, if your career goal is "I want to become an IC leader", your SMART goal can concentrate around developing new technical skills, completing a course, or becoming a mentor to a junior peer.
- You can use this [FigJam template](https://www.figma.com/community/file/1104227437175207230) to help set your SMART goals.

<!-- Consider using the following format: `(I or accountable party) will (action word/s) (object of the goal) by (time), in order to
(relevance/results).`

Example: I will complete and earn a certification in Crucial Conversations by June 31, 2024, in order to apply my conversation skills when having difficult conversations with my team and counterparts. -->

### SMART goal 1

- Career goal:
- SMART goal:
    - S:
    - M:
    - A:
    - R:
    - T:
- Outcome:
- Deadline:

### SMART goal 2

- Career goal:
- SMART goal:
    - S:
    - M:
    - A:
    - R:
    - T:
- Outcome:
- Deadline:

## Checkpoint

- [ ] Individually, take some hours to reflect on your career development goals and create your SMART goals.
- [ ] Schedule a call (30/45 min duration) to review and discuss with your manager.
- [ ] Update your SMART goals as needed.
- [ ] Schedule recurring monthly check-in calls (30/45 min duration) with your manager to discuss your IGP and career development progress.
- [ ] At the end of the fiscal year in January, consider doing a retrospective to reflect on your goals and progress made.
